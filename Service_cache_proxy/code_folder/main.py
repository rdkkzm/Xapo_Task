# built in imports
import json
import os
from typing import Optional

import requests

# pip3 install imports
from fastapi import FastAPI, Request, Response, status
from fastapi.responses import JSONResponse
from fastapi_utils.tasks import repeat_every
from pydantic import BaseSettings

# user created modules
from Common_utilities.settings_utils import SettingsUtils
from Service_cache_proxy.code_folder.cache import Cache


class Settings(BaseSettings, SettingsUtils):
    HEALTH_CHECK_URL = os.getenv("HEALTH_CHECK_URL", False)

    CACHE_HOST: str = os.getenv("CACHE_HOST", False)
    CACHE_PORT: int = os.getenv("CACHE_PORT", False)
    CACHE_DB_NAME_1: str = os.getenv("CACHE_DB_NAME_1", False)


settings = Settings()
cache = Cache(host=settings.CACHE_HOST, port=settings.CACHE_PORT, db_name=settings.CACHE_DB_NAME_1)
app = FastAPI()
app.on_event("startup")(settings.check_env_vars)


@app.on_event("startup")
@repeat_every(seconds=60)
async def status_report():
    requests.post(f"{settings.HEALTH_CHECK_URL}/report?level=info", data="API_CACHE_PROXY_main.py:API proxy alive")


@app.get("/record")
async def get_record() -> JSONResponse(status_code=200 or 204 or 501 or 503):
    response = cache.get_record_from_cache()
    if response.status_code == 200:
        requests.post(f"{settings.HEALTH_CHECK_URL}/report?level=debug", data=f"API_CACHE_main.py:200 {response.body}")
        return JSONResponse(status_code=status.HTTP_200_OK, content=json.loads(response.body))
    requests.post(
        f"{settings.HEALTH_CHECK_URL}/report?level=error",
        data=f"API_CACHE_main.py:got {response.status_code} from redis",
    )
    return JSONResponse(status_code=response.status_code)


@app.post("/add/")
async def add_to_cache(request: Request, ttl: Optional[int] = 60) -> JSONResponse(status_code=201 or 501 or 503):
    input_json = await request.json()
    response = cache.add2cache(value=input_json, ttl_sec=ttl)
    if response.status_code == 201:
        requests.post(f"{settings.HEALTH_CHECK_URL}/report?level=debug", data="API_CACHE_main.py:201 Created")
        return Response(status_code=status.HTTP_200_OK, content=json.loads(response.body))
    requests.post(
        f"{settings.HEALTH_CHECK_URL}/report?level=error",
        data=f"API_CACHE_main.py:got {response.status_code} from redis",
    )
    return JSONResponse(status_code=response.status_code)


if __name__ == "__main__":
    import uvicorn

    uvicorn.run(app, host="0.0.0.0", port=9002)

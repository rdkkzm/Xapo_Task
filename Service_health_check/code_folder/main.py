# pip3 install imports
from fastapi import FastAPI, Request, status

app = FastAPI()


@app.get("/")
async def root():
    return status.HTTP_200_OK


@app.post("/report")
async def report(level: str, request: Request):
    # @TODO JIRA-Ticket logging to https://sentry.io/welcome/
    response = (await request.body()).decode("UTF-8")
    if level == "debug":
        print(f"{level}:{response}")
        return status.HTTP_200_OK
    if level == "info":
        print(f"{level}:{response}")
        return status.HTTP_200_OK
    if level == "warning":
        print(f"\n{level}:{response}")
        return status.HTTP_200_OK
    if level == "error":
        print(f"\n\n{'#' * 30}\n {level}:{response}\n{'#' * 30}")
        return status.HTTP_200_OK
    if level == "critical":
        print(f"\n\n{'#' * 30}\n {level}:{response}\n{'#' * 30}")
        return status.HTTP_200_OK
    return status.HTTP_501_NOT_IMPLEMENTED


if __name__ == "__main__":
    import uvicorn

    uvicorn.run(app, host="0.0.0.0", port=9050)
